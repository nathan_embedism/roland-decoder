/************************************************************************/
/*																		*/
/*	IOXP.cpp		--		Definition for IOXP library 	    		*/
/*																		*/
/************************************************************************/
/*	Author:		Cristian Fatu											*/
/*	Copyright 2012, Digilent Inc.										*/
/************************************************************************/
/*  File Description:													*/
/*		This file defines functions for IOXP							*/
/*																		*/
/************************************************************************/
/*  Revision History:													*/
/*																		*/
/*	01/10/2012(Cristian Fatu): created	                                */
/*	06/19/2015(JonP) Revision	                                        */
/************************************************************************/
/*	Needs work:															*/
/*																		*/
/*		Various functions are not fully working or could be added       */
/*      in the library such as:                                         */
/*       - The lock and unlock for the FIFO is not fully working        */
/*		 - Add more functionality with the Logic Blocks                 */
/*		 - Add more functionality with the pull-up and pull-down        */
/*         Resistors                                                    */								
/*		 - Add more functionality with the Clock divider                */
/*																		*/
/************************************************************************/
/*                                                                      */
/*  This library is free software; you can redistribute it and/or       */
/*  modify it under the terms of the GNU Lesser General Public          */
/*  License as published by the Free Software Foundation; either        */
/*  version 2.1 of the License, or (at your option) any later version.  */
/*                                                                      */
/*  This library is distributed in the hope that it will be useful,     */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   */
/*  Lesser General Public License for more details.                     */
/*                                                                      */
/*  You should have received a copy of the GNU Lesser General Public    */ 
/*  License along with this library; if not, write to the Free Software */
/*  Foundation, Inc.,51 Franklin St, Fifth Floor, Boston, MA  02110-1301*/ 
/*  USA                                                                 */
/************************************************************************/

/* -------------------------------------------------------------------- */
/*				Include File Definitions						        */
/* -------------------------------------------------------------------- */
#include "ioxp.h"
#include "main.h"

static I2C_HandleTypeDef _hi2c;

void IOXP_init(I2C_HandleTypeDef * hi2c)
{
	_hi2c = *hi2c;


}

/* -------------------------------------------------------------------- */
/*	IOXP::WriteBytesI2C                                                 */
/*	Synopsis:                                                           */
/*		WriteBytesI2C(bRegisterAddress, bCntBytes,  rgbValues);         */
/*	Parameters:                                                         */  
/*		uint8_t bAddress   – the address where the values will be       */
/*                           written                                    */
/*  	uint8_t bCntBytes  – the number of bytes that will be written   */
/*		uint8_t *rgbValues - the array of values to be written          */
/*                                                                      */	                                                     
/*  Return Value:                                                       */
/*		void                                                            */
/*                                                                      */
/*	Errors:                                                             */
/*                                                                      */
/*	Description:                                                        */
/*		This function writes the values from the buffer to the          */ 
/*  	specified number of registers starting from a specified address */
/*		value. It performs the I2C write cycle for the specified array  */ 
/*  	of values to the specified address.                             */
/* -------------------------------------------------------------------- */

void IOXP_WriteBytesI2C(uint8_t bAddress, uint8_t bCntBytes, uint8_t *rgbValues)
{
	uint8_t _data[bCntBytes+1];

	_data[0] = bAddress;

	for (uint8_t i = 0; i < (bCntBytes+1); i++) {
		_data[i+1] = rgbValues[i];
	}

	//HAL_I2C_Master_Transmit(&_hi2c, IOXP_I2C_ADDR,bAddress,1,HAL_MAX_DELAY);


	if(HAL_I2C_Master_Transmit(&_hi2c, IOXP_I2C_ADDR,_data,bCntBytes+1,HAL_MAX_DELAY) != HAL_OK)
	{
		//error
		volatile int z = 0;
	}

	/*
    Wire.beginTransmission(IOXP_I2C_ADDR); //start transmission to device 
    Wire.send(bAddress);        // send register address
	int nIdxBytes;
	for(nIdxBytes = 0; nIdxBytes < bCntBytes; nIdxBytes++)
	{
		Wire.send(rgbValues[nIdxBytes]); // send value to write
	}	
	Wire.endTransmission(); //end transmission
	*/
}

/* -------------------------------------------------------------------- */
/*	IOXP::ReadBytesI2C                                                  */
/*                                                                      */
/*	Synopsis:                                                           */
/*	   ReadBytesI2C(bRegisterAddress, bCntBytes, rgbValues);            */
/*	Parameters:                                                         */  
/*	   uint8_t bAddress   – the address from where the values will be   */
/*	                        read                                        */
/*	   uint8_t bCntBytes  – the number of bytes that will be read       */
/*	   uint8_t *rgbValues - the array where values will be read         */
/*                                                                      */	                                                     
/*  Return Value:                                                       */
/*		void                                                            */
/*                                                                      */
/*	Errors:                                                             */
/*                                                                      */
/*	Description:                                                        */
/*		This function will read the specified number of registers       */ 
/*  	starting from a specified address and store their values in the */
/*		buffer. It performs the I2C read cycle from the specified       */ 
/*  	address into the specified array of values.                     */
/* -------------------------------------------------------------------- */

void IOXP_ReadBytesI2C(uint8_t bAddress, uint8_t bCntBytes, uint8_t *rgbValues)
{      
	if(HAL_I2C_Master_Transmit(&_hi2c, IOXP_I2C_ADDR,&bAddress,1,HAL_MAX_DELAY) != HAL_OK)
	{
		//error
		volatile int z = 0;
	}

	if(HAL_I2C_Master_Receive(&_hi2c, IOXP_I2C_ADDR, rgbValues, bCntBytes, HAL_MAX_DELAY) != HAL_OK)
	{
		//error
		volatile int z = 0;
	}
}

/* -------------------------------------------------------------------- */
/*	IOXP::SetKeyboardPinConfig                                          */
/*                                                                      */
/*	Synopsis:                                                           */
/*		SetKeyboardPinConfig(uint8_t bRowCfg, uint16_t wColCfg);        */
/*	Parameters:                                                         */
/*	 	uint8_t bRowCfg		  											*/
/*		- one byte containing keyboard rows configuration               */
/*				(Rows 0-7), as a bitmap:                                */
/*			- if bit x from the byte is set, then the pin corresponding */
/*				to row x is used for keyboard.                          */
/*		- if bit x from the byte is not set, then the pin               */
/*				corresponding to row x is not used for keyboard         */
/*				(used as GPIO).                                         */
/*		uint16_t wColCfg	                                            */
/*			- two bytes containing keyboard columns configuration       */
/*				(Columns 0-10), as a bitmap:                            */
/*     		-if bit x of one of the two bytes is set, then the pin      */
/*				corresponding to column x is used for keyboard.         */
/*			- if bit x is not set, then pin corresponding to column     */
/*					x is not used for keyboard (used as GPIO).          */
/*		                                                                */
/*  Return Value:                                                       */
/*		void                                                            */
/*                                                                      */
/*	Errors:                                                             */
/*                                                                      */
/*	Description:                                                        */
/*		This function configures the IO pins to be used for keyboard.   */
/*		The input parameters describe rows and columns configuration,   */
/*		as bitmaps containing 1 value for rows / columns used by the    */
/*		keyboard                                                        */
/* -------------------------------------------------------------------- */

void IOXP_SetKeyboardPinConfig(uint8_t bRowCfg, uint16_t wColCfg)
{
	uint8_t rgbVals[3];
	rgbVals[0] = bRowCfg;	// rows 0 - 7
	rgbVals[1] = ((uint8_t)(wColCfg & 0x0F));	// cols 0 - 7
	rgbVals[2] = ((uint8_t)((wColCfg & 0x700) >> 8));	// cols 8 - 10
	IOXP_WriteBytesI2C(IOXP_ADDR_PIN_CONFIG_A, 3, rgbVals);
}



void SetRegisterBit(uint16_t wBitDef, uint8_t bBitVal)
{
	uint8_t bRegisterAddr = *((uint8_t *)&wBitDef + 1);
	uint8_t bMask = *(uint8_t *)&wBitDef;
	WriteMaskedRegisterValue(bRegisterAddr, bMask, (bBitVal != 0) ? 0xFF: 0);
}

uint8_t IOXP_GetRegisterBit(uint16_t wBitDef)
{
	uint8_t bRegisterAddr = *((uint8_t *)&wBitDef + 1);
	uint8_t bMask = *(uint8_t *)&wBitDef;
	return ReadMaskedRegisterValue(bRegisterAddr, bMask) != 0;
}

uint8_t IOXP_GetRegisterBitsGroup(uint16_t wBitDef)
{
	uint8_t bRegisterAddr = *((uint8_t *)&wBitDef + 1);
	uint8_t bMask = *(uint8_t *)&wBitDef;
	uint8_t bScale = Mask2Scale(bMask);
	uint8_t bGroupVal = ReadMaskedRegisterValue(bRegisterAddr, bMask) >> bScale;
	return bGroupVal;
}

void SetRegisterBitsGroup(uint16_t wBitDef, uint8_t bBitsVal)
{
	uint8_t bRegisterAddr = *((uint8_t *)&wBitDef + 1);
	uint8_t bMask = *(uint8_t *)&wBitDef;
	uint8_t bScale = Mask2Scale(bMask);
	uint8_t bScaledGroupVal = bBitsVal << bScale;
	WriteMaskedRegisterValue(bRegisterAddr, bMask, bScaledGroupVal);
}

uint32_t IOXP_GetGPIStat()
{
	uint32_t bResult = 0;
	IOXP_ReadBytesI2C(IOXP_ADDR_GPI_STATUS_A, 3, (uint8_t *)&bResult);
	return bResult;
}

//void IOXP_ReadFIFO(int &iKeyVal, uint8_t &bRow, uint8_t &bCol, uint8_t &bGPI, uint8_t &bLogic, uint8_t &bEventState)
//{
//	uint8_t bEvent;
//	IOXP_ReadBytesI2C(IOXP_ADDR_FIFO1, 1, &bEvent);
//	DecodeEvent(bEvent, iKeyVal, bRow, bCol, bGPI, bLogic, bEventState);
//}

void WriteMaskedRegisterValue(uint8_t bAddress, uint8_t bMask, uint8_t bVal)
{
	uint8_t bOldVal;
	IOXP_ReadBytesI2C(bAddress, 1, &bOldVal);
	uint8_t bNewVal = (bOldVal & ~bMask) | (bVal & bMask);
	IOXP_WriteBytesI2C(bAddress, 1, &bNewVal);
}

uint8_t ReadMaskedRegisterValue(uint8_t bAddress, uint8_t bMask)
{
	uint8_t bVal;
	IOXP_ReadBytesI2C(bAddress, 1, &bVal);
	return bVal & bMask;
}

uint8_t Mask2Scale(uint8_t bMask)
{
	uint8_t bResult = 0;
	uint8_t bVal = bMask;
	while((bMask & 1) == 0)
	{
		bMask >>= 1;
		bResult++;
	}
	return bResult;
}

void SetGPIODirection(uint32_t dwBitMap)
{
	IOXP_WriteBytesI2C(IOXP_ADDR_GPIO_DIRECTION_A, 3, (uint8_t *)&dwBitMap);
}
