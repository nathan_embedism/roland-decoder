/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "ioxp.h"
#include <stdbool.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_I2C1_Init(void);

/* USER CODE BEGIN PFP */
void i2c_bus_scan();
void enable_a_bank();
void enable_b_bank();
uint8_t increment_scan_line(bool one_channel);
uint8_t set_scan_line(uint8_t _line);
uint8_t set_scan_bit(uint8_t _line);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART3_UART_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */

  	uint8_t Buffer[25] = {0};
	uint8_t Space[] = " - ";

	uint8_t i = 0, ret;

    IOXP_init(&hi2c1);								// pass i2c handler into ioxp driver
    uint8_t ret_val = 0;

    IOXP_ReadBytesI2C(IOXP_ADDR_ID, 1, &ret_val);	// check comms with the ioxp by reading it's manufacturer and revision ID
    sprintf(Buffer, "Man and Rev: 0x%X\r\n", ret_val);
    HAL_UART_Transmit(&huart3, Buffer, sizeof(Buffer), 10000);
    sprintf(Buffer, "                      ");

    // enable oscillator
    SetRegisterBit(IOXP_GENERAL_CFG_B_OSC_EN,  1);

    // ensure all set as GPIO not scanning
    uint8_t conf_a = 0b00000000;
    IOXP_WriteBytesI2C(IOXP_ADDR_PIN_CONFIG_A, 1, &conf_a);
    IOXP_WriteBytesI2C(IOXP_ADDR_PIN_CONFIG_B, 1, &conf_a);

    // setup direction of pins
    conf_a = 0b00000000; // 1-8 inputs
    uint8_t conf_b = 0b11111111; // 9-16 outputs
    IOXP_WriteBytesI2C(IOXP_ADDR_GPIO_DIRECTION_A, 1, &conf_a);
    IOXP_WriteBytesI2C(IOXP_ADDR_GPIO_DIRECTION_B, 1, &conf_b);

    // configure pulldowns
    conf_b = 0b01010101;
    IOXP_WriteBytesI2C(IOXP_ADDR_RPULL_CONFIG_A, 1, &conf_b);
    IOXP_WriteBytesI2C(IOXP_ADDR_RPULL_CONFIG_B, 1, &conf_b);
    IOXP_WriteBytesI2C(IOXP_ADDR_RPULL_CONFIG_C, 1, &conf_b);
    IOXP_WriteBytesI2C(IOXP_ADDR_RPULL_CONFIG_D, 1, &conf_b);

    /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
	  uint8_t OBuffer[128] = {0};

	  for( uint8_t i = 0 ; i<4 ; i++ )
	  {
		  set_scan_line(i);
		  IOXP_ReadBytesI2C(IOXP_ADDR_GPI_STATUS_A, 1, &ret_val);

		  if(ret_val){
			  sprintf(OBuffer, "%d "BYTE_TO_BINARY_PATTERN"                ", i, BYTE_TO_BINARY(ret_val));
			  HAL_UART_Transmit(&huart3, OBuffer, sizeof(Buffer), 10000);
		  }

		  if(ret_val){
			  set_scan_line(i+4);
			  IOXP_ReadBytesI2C(IOXP_ADDR_GPI_STATUS_A, 1, &ret_val);
			  sprintf(OBuffer, "%d "BYTE_TO_BINARY_PATTERN"                ", i+4, BYTE_TO_BINARY(ret_val));
			  HAL_UART_Transmit(&huart3, OBuffer, sizeof(Buffer), 10000);

			  volatile uint8_t z = 0;
		  }
	  }
	  sprintf(OBuffer, "\r\n                                     ");
	  HAL_UART_Transmit(&huart3, OBuffer, sizeof(Buffer), 10000);
  }

  while (1)
  {
	  uint8_t OBuffer[128] = {0};

	  IOXP_ReadBytesI2C(IOXP_ADDR_GPI_STATUS_A, 1, &ret_val);
	  uint8_t line = increment_scan_line(ret_val);

	  switch(line){
	  // Channel B
	  	  case 1:
	  		  sprintf(OBuffer, "1   "BYTE_TO_BINARY_PATTERN"                ", BYTE_TO_BINARY(ret_val));
	  		  break;
	  	  case 2:
	  		  sprintf(OBuffer, "2   "BYTE_TO_BINARY_PATTERN"                ", BYTE_TO_BINARY(ret_val));
	  		  break;

	  	  case 4:
	  		  sprintf(OBuffer, "4   "BYTE_TO_BINARY_PATTERN"                ", BYTE_TO_BINARY(ret_val));
	  		  break;
	  	  case 8:
	  		  sprintf(OBuffer, "8   "BYTE_TO_BINARY_PATTERN"\r\n", BYTE_TO_BINARY(ret_val));
	  		  break;

	  	  // Channel A
	  	  case 16:
	  		  sprintf(OBuffer, "16  "BYTE_TO_BINARY_PATTERN"                ", BYTE_TO_BINARY(ret_val));
	  		  break;
	  	  case 32:
	  		  sprintf(OBuffer, "32  "BYTE_TO_BINARY_PATTERN"                ", BYTE_TO_BINARY(ret_val));
	  		  break;

	  	  case 64:
	  		  sprintf(OBuffer, "64  "BYTE_TO_BINARY_PATTERN"                ", BYTE_TO_BINARY(ret_val));
	  		  break;

	  	  case 128:
	  		  sprintf(OBuffer, "128 "BYTE_TO_BINARY_PATTERN"\r\n", BYTE_TO_BINARY(ret_val));
	  		  break;
	  }


	  //sprintf(Buffer, "%d: 0x%X\t", line, ret_val);
	  HAL_UART_Transmit(&huart3, OBuffer, sizeof(Buffer), 10000);



    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_LDO_SUPPLY);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 24;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV1;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_I2C1;
  PeriphClkInitStruct.Usart234578ClockSelection = RCC_USART234578CLKSOURCE_D2PCLK1;
  PeriphClkInitStruct.I2c123ClockSelection = RCC_I2C123CLKSOURCE_D2PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x10B0DCFB;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart3, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart3, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LD3_Pin|LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(USB_PowerSwitchOn_GPIO_Port, USB_PowerSwitchOn_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : USER_Btn_Pin */
  GPIO_InitStruct.Pin = USER_Btn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USER_Btn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_MDC_Pin RMII_RXD0_Pin RMII_RXD1_Pin */
  GPIO_InitStruct.Pin = RMII_MDC_Pin|RMII_RXD0_Pin|RMII_RXD1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_REF_CLK_Pin RMII_MDIO_Pin RMII_CRS_DV_Pin */
  GPIO_InitStruct.Pin = RMII_REF_CLK_Pin|RMII_MDIO_Pin|RMII_CRS_DV_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : RMII_TXD1_Pin */
  GPIO_InitStruct.Pin = RMII_TXD1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(RMII_TXD1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD3_Pin LD2_Pin */
  GPIO_InitStruct.Pin = LD3_Pin|LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = USB_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(USB_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_OverCurrent_Pin */
  GPIO_InitStruct.Pin = USB_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB_OverCurrent_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : USB_SOF_Pin USB_ID_Pin USB_DM_Pin USB_DP_Pin */
  GPIO_InitStruct.Pin = USB_SOF_Pin|USB_ID_Pin|USB_DM_Pin|USB_DP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG1_FS;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_TX_EN_Pin RMII_TXD0_Pin */
  GPIO_InitStruct.Pin = RMII_TX_EN_Pin|RMII_TXD0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void i2c_bus_scan()
{
	uint8_t Buffer[25] = {0};
	uint8_t Space[] = " - ";
	uint8_t StartMSG[] = "Starting I2C Scanning: \r\n";
	uint8_t EndMSG[] = "Done! \r\n\r\n";

	uint8_t i = 0, ret;

	  /*-[ I2C Bus Scanning ]-*/
	  HAL_UART_Transmit(&huart3, StartMSG, sizeof(StartMSG), 10000);
	  for(i=1; i<128; i++)
	  {
	  	ret = HAL_I2C_IsDeviceReady(&hi2c1, (uint16_t)(i<<1), 3, 5);
	  	if (ret != HAL_OK) /* No ACK Received At That Address */
	  	{
	  	    HAL_UART_Transmit(&huart3, Space, sizeof(Space), 10000);
	      }
	  	else if(ret == HAL_OK)
	  	{
	  	    sprintf(Buffer, "0x%X", i);
	  	    HAL_UART_Transmit(&huart3, Buffer, sizeof(Buffer), 10000);
	  	}
	  	HAL_GPIO_TogglePin(GPIOB, LD3_Pin);
	  }
	  HAL_UART_Transmit(&huart3, EndMSG, sizeof(EndMSG), 10000);
	  /*--[ Scanning Done ]--*/
}

void enable_a_bank()
{
	// a bank
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(9), 1);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(10), 1);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(11), 1);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(12), 1);
	// b bank
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(13), 0);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(14), 0);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(15), 0);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(16), 0);
}

void enable_b_bank()
{
	// a bank
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(9), 0);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(10), 0);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(11), 0);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(12), 0);
	// b bank
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(13), 1);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(14), 1);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(15), 1);
	SetRegisterBit(IOXP_GPO_DATA_OUT_GPO_DATA(16), 1);
}

uint8_t increment_scan_line(bool one_channel)
{
	static uint8_t count = 1;
	static bool one_chan = false;

	one_chan |= one_channel;

	IOXP_WriteBytesI2C(IOXP_ADDR_GPO_DATA_OUT_B, 1, &count);
	uint8_t ret = count;

	count = 2 * count;
	if(!one_chan && (count > 8)) {
		count = 1;
		one_chan = false;
	}
	if(count == 0){
		count = 1;
		one_chan = false;
	}

	return ret;
}

uint8_t set_scan_line(uint8_t _line)
{
	uint8_t line = 1;
	line = line << (_line);
	set_scan_bit(line);
	return _line;
}

uint8_t set_scan_bit(uint8_t _line)
{
	IOXP_WriteBytesI2C(IOXP_ADDR_GPO_DATA_OUT_B, 1, &_line);
	return _line;
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
